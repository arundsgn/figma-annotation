## Project notes

A simple figma plugin to attach notes to Figma frame.

How it works:
1. Open the plugin
2. Select the frame
3. Add the annotation
4. Add the optional tag

🎉 A note card will be created adjacent to the frame along the author name.

Maximum characters: 700

Next feature on pipeline:
1. Add note date 
2. Group the the cards based on the tag
3. Identify links and make it clickable

Link to install the figma plugin: https://www.figma.com/community/plugin/1103354597235211595





