
figma.showUI(__html__);
figma.ui.resize(180, 340);
let xValue = 0;
let yValue = 0;
let widthValue = 0;
let time = new Date().toLocaleString();

const node = figma.currentPage.selection[0]

const myFontLoadingFunction = async () => {
    await figma.loadFontAsync({ family: "Inter", style: "Bold" })
    await figma.loadFontAsync({ family: "Inter", style: "Regular" })
    }

function* walkTree(node) {
    yield node;
    let children = node.children;
    if (children) {
        for (let child of children) {
        yield* walkTree(child)
        }
    }
    }

figma.ui.onmessage = msg => {

    if (msg.type === 'createNoteEvent' && figma.currentPage.selection.length > 0) {
        xValue = figma.currentPage.selection[0].x;
        widthValue = figma.currentPage.selection[0].width;
        yValue = figma.currentPage.selection[0].y;
        
        const nodes = [];
        
        const rect = figma.createRectangle();
        rect.x = xValue + widthValue + 20;
        rect.y = yValue;

        switch (msg.color) {
            case "yellow":
                rect.fills = [{ type: 'SOLID', color: { r: 0.992, g: 1, b: 0.6784 } }];
                break;
            case "biscuit":
                rect.fills = [{ type: 'SOLID', color: { r: 1, g: 0.796, b: 0.6784 } }];
                break;
            case "sky":
                rect.fills = [{ type: 'SOLID', color: { r: 0.6784, g: 0.941176, b: 1 } }];
                break;
            case "purple":
                rect.fills = [{ type: 'SOLID', color: { r: 0.6784, g: 0.7098, b: 1 } }];
                break;
            case "pink":
                rect.fills = [{ type: 'SOLID', color: { r: .97255, g: 0.6784, b: 1 } }];
                break;
        }

        rect.resize(180, 180);
        rect.bottomRightRadius = 32;
        rect.bottomLeftRadius = 4;
        rect.topRightRadius = 4;

        figma.currentPage.appendChild(rect);
        nodes.push(rect);
        
        myFontLoadingFunction().then(() => {
            const myTextLayer = figma.createText();
            const userName = figma.createText();
            const displayTime = figma.createText();

            myTextLayer.fontName = { family: "Inter", style: "Regular" };
            myTextLayer.characters = msg.noteMessage;
            myTextLayer.resize(160, 140);
            myTextLayer.x = xValue + widthValue + 32;
            myTextLayer.y = yValue +20;

            figma.currentPage.appendChild(myTextLayer);
            nodes.push(myTextLayer);

            if(msg.noteMessage.length < 200) {
                myTextLayer.fontSize = 13;
            } 
            else  if(msg.noteMessage.length > 200 && msg.noteMessage.length < 300) {
                myTextLayer.fontSize = 11;
            } 
            else {
                myTextLayer.fontSize = 7;
            }

            userName.fontName = { family: "Inter", style: "Bold" };
            userName.characters = "👤  "+ figma.currentUser.name; 
            userName.fontSize = 9;
            userName.x = xValue + widthValue + 32;
            userName.y = yValue + 150;
            figma.currentPage.appendChild(userName);
            nodes.push(userName);

            // userImage.fills

            displayTime.fontName = { family: "Inter", style: "Regular" };
            displayTime.characters = "🕛    "+ time; 
            displayTime.fontSize = 6;
            displayTime.x = xValue + widthValue + 32;
            displayTime.y = yValue + 165;
            figma.currentPage.appendChild(displayTime);
            nodes.push(displayTime);
            figma.group(nodes, figma.currentPage).name = "PN_____"+time+"_"+figma.currentUser.name; // Name of the note created with PN and 5*"_"
            //figma.group(nodes, node.parent)
            })

        figma.currentPage.selection = nodes;
        figma.viewport.scrollAndZoomIntoView(nodes);
    } 
    
    else if (msg.type === 'searchNoteEvent') {   
        let walker = walkTree(figma.root)
        let res;
        let results = [];
        while (!(res = walker.next()).done) {   
            let node = res.value;
            if (node.type === 'GROUP' && node.name.substr(0, 7) === "PN_____") {
                node.children.forEach(element => {
                    if(element.type === 'TEXT') { 
                        if (element.characters.substr(0,2).codePointAt(0) != 128100 && element.characters.substr(0,2).codePointAt(0) != 128347) { //Checking for elements starting with emoji
                            const noteText = element.characters.toLowerCase();
                            const keywordLower = msg.keyword.toLowerCase();
                            if (noteText.includes(keywordLower)) { 
                                results.push({"id": node.id, "name": node.children[1].characters, "author": node.children[2].characters, "time": node.children[3].characters, "xValue": node.x, "yValue":node.y});
                            } 
                        } 
                    }
                });
            }
        }
        figma.ui.postMessage({results});
    } 
    else if (msg.type === 'goToNoteEvent') {
        let nodesToBeDisplayed = [];
        let parentId = figma.getNodeById(msg.evtID).parent.id;
        
        figma.currentPage = figma.getNodeById(parentId)

        nodesToBeDisplayed.push(figma.getNodeById(msg.evtID));
        figma.viewport.scrollAndZoomIntoView(nodesToBeDisplayed);
    }

    else {
        figma.notify("🤷‍♂️ Please select a frame");
        return;
    }
};
